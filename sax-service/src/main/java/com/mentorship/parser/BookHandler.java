package com.mentorship.parser;

import com.mentorship.model.Book;
import com.mentorship.model.Catalog;
import com.mentorship.model.DateUtil;
import lombok.Getter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Created by Maks on 13.11.2016.
 */
public class BookHandler extends DefaultHandler {

    @Getter
    private Catalog catalog;

    private Stack<String> elementStack = new Stack<String>();
    private Stack<Object> objectStack = new Stack<Object>();

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        this.elementStack.push(qName);

        if (qName.equals("catalog")) {
            catalog = new Catalog();
        } else if (qName.equals("book")) {
            Book book = new Book();
            book.setId(attributes.getValue("id"));
            objectStack.push(book);
            if (catalog.getBooksList() == null) {
                catalog.setBooksList(new ArrayList<>());
            }
        }

    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {

        String value = new String(ch, start, length).trim();
        if (value.length() == 0) return;

        if (currentElement().equals("title") && currentElementParent().equals("catalog")) {
            catalog.setTitle(value);
        } else if (currentElement().equals("author") && currentElementParent().equals("book")) {
            Book book = (Book) this.objectStack.peek();
            book.setAuthor(value);
        } else if (currentElement().equals("title") && currentElementParent().equals("book")) {
            Book book = (Book) this.objectStack.peek();
            book.setTitle(value);
        } else if (currentElement().equals("genre") && currentElementParent().equals("book")) {
            Book book = (Book) this.objectStack.peek();
            book.setGenre(value);
        } else if (currentElement().equals("price") && currentElementParent().equals("book")) {
            Book book = (Book) this.objectStack.peek();
            book.setPrice(Float.parseFloat(value));
        } else if (currentElement().equals("publish_date") && currentElementParent().equals("book")) {
            Book book = (Book) this.objectStack.peek();
            book.setPublish(DateUtil.parseDate(value));
        } else if (currentElement().equals("description") && currentElementParent().equals("book")) {
            Book book = (Book) this.objectStack.peek();
            book.setDescription(value);
        }

    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        this.elementStack.pop();
        if (qName.equals("book")) {
            Book book = (Book) this.objectStack.pop();
            catalog.getBooksList().add(book);
        }

    }

    private String currentElement() {
        return this.elementStack.peek();
    }

    private String currentElementParent() {
        if (this.elementStack.size() < 2) return null;
        return this.elementStack.get(this.elementStack.size() - 2);
    }
}
