package com.mentorship.controller;

import com.mentorship.model.Catalog;
import com.mentorship.parser.BookStaxParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Maks on 13.11.2016.
 */
@RestController
@RequestMapping("/stax")
public class StaxRestController {

    @Autowired
    private BookStaxParser bookStaxParser;

    @GetMapping("/catalog")
    public Catalog getCatalog() {
        return bookStaxParser.parseXmlCatalog();
    }
}
