package com.mentorship.parser;

import com.mentorship.model.Book;
import com.mentorship.model.Catalog;
import com.mentorship.model.DateUtil;
import org.springframework.stereotype.Component;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Maks on 13.11.2016.
 */
@Component
public class BookStaxParser {

    private static String filePath = "xml/xml_task.xml";

    public Catalog parseXmlCatalog() {
        Catalog catalog = null;
        XMLInputFactory factory = XMLInputFactory.newInstance();
        try (InputStream xmlInput = BookStaxParser.class.getClassLoader().getResourceAsStream(filePath)) {
            XMLStreamReader reader = factory.createXMLStreamReader(xmlInput);
            catalog = processXmlCatalog(reader);
        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
        }
        return catalog;
    }

    private Catalog processXmlCatalog(XMLStreamReader reader) throws XMLStreamException {
        Catalog catalog = null;
        Book book = null;
        String value = null;
        String parentElement = null;

        while (reader.hasNext()) {
            int event = reader.next();
            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    if (reader.getLocalName().equals("catalog")) {
                        catalog = new Catalog();
                        parentElement = reader.getLocalName();
                    } else if (reader.getLocalName().equals("booksList")) {
                        catalog.setBooksList(new ArrayList<>());
                    } else if (reader.getLocalName().equals("book")) {
                        parentElement = reader.getLocalName();
                        book = new Book();
                        book.setId(reader.getAttributeValue(0));
                    }


                    break;

                case XMLStreamConstants.CHARACTERS:
                    value = reader.getText().trim();
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    if (reader.getLocalName().equals("title") && parentElement.equals("catalog")) {
                        catalog.setTitle(value);
                    } else if (reader.getLocalName().equals("title") && parentElement.equals("book")) {
                        book.setTitle(value);
                    } else if (reader.getLocalName().equals("author") && parentElement.equals("book")) {
                        book.setAuthor(value);
                    } else if (reader.getLocalName().equals("publish_date") && parentElement.equals("book")) {
                        book.setPublish(DateUtil.parseDate(value));
                    } else if (reader.getLocalName().equals("description") && parentElement.equals("book")) {
                        book.setDescription(value);
                    } else if (reader.getLocalName().equals("price") && parentElement.equals("book")) {
                        book.setPrice(Float.parseFloat(value));
                    } else if (reader.getLocalName().equals("genre") && parentElement.equals("book")) {
                        book.setGenre(value);
                    } else if (reader.getLocalName().equals("book")) {
                        catalog.getBooksList().add(book);
                    }

                    break;

            }
        }

        return catalog;
    }

}
