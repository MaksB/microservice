package com.mentorship.parser;


import com.mentorship.model.Catalog;
import org.springframework.stereotype.Component;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXSource;
import net.sf.saxon.Configuration;
import net.sf.saxon.s9api.*;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Maks on 13.11.2016.
 */
@Component
public class BookSAXParser {

    private static String filePath = "xml/xml_task.xml";

    public Catalog parseCatalogXml() {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        BookHandler bookHandler = new BookHandler();
        SAXParser saxParser = null;

        try (InputStream xmlInput = BookSAXParser.class.getClassLoader().getResourceAsStream(filePath)) {
            saxParser = factory.newSAXParser();
            saxParser.parse(xmlInput, bookHandler);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }


        return bookHandler.getCatalog();
    }

    public String getBooksByPrice() {
        String result = null;
        try (InputStream inputStream = BookSAXParser.class.getClassLoader().getResourceAsStream(filePath)) {

            Configuration saxonConfig = new Configuration();
            Processor processor = new Processor(saxonConfig);

            XQueryCompiler xqueryCompiler = processor.newXQueryCompiler();
            XQueryExecutable xqueryExec = xqueryCompiler
                    .compile("<booksList>{"
                            + "for $x in /catalog/booksList/book\n" +
                            "        where $x/price>5.95\n" +
                            "        return $x"
                            + "}</booksList>");

            XQueryEvaluator xqueryEval = xqueryExec.load();
            xqueryEval.setSource(new SAXSource(new InputSource(inputStream)));

            XdmDestination destination = new XdmDestination();

            xqueryEval.setDestination(destination);

            xqueryEval.run();

            result = destination.getXdmNode().toString();

        } catch (Exception e) {
            e.printStackTrace();
        }


        return result;
    }
}
