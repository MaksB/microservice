package com.mentorship.parser;

import com.mentorship.model.Book;
import com.mentorship.model.Catalog;
import com.mentorship.model.DateUtil;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Maks on 12.11.2016.
 */
@Component
public class BookDOMParser {

    private static String filePath = "xml/xml_task.xml";
    private static String xPathExpression = "catalog/booksList/book[contains(author,\"O'Brien, Tim\")]";

    public Catalog parseFile() {
        Catalog catalog;
        Document document = getXmlDocument();
        catalog = convertToObject(document);

        return catalog;
    }

    private Catalog convertToObject(Document document) {
        Catalog catalog = new Catalog();

        NodeList nodeList = document.getDocumentElement().getChildNodes();
        catalog.setTitle(nodeList.item(1).getTextContent());
        nodeList = document.getElementsByTagName("book");
        catalog.setBooksList(parseBook(nodeList));

        return catalog;
    }

    private Collection<Book> parseBook(NodeList nodeList) {
        Collection<Book> books = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                Book book = new Book();
                book.setId(element.getAttribute("id"));
                book.setAuthor(element.getElementsByTagName("author").item(0).getTextContent());
                book.setTitle(element.getElementsByTagName("title").item(0).getTextContent());
                book.setDescription(element.getElementsByTagName("description").item(0).getTextContent());
                book.setGenre(element.getElementsByTagName("genre").item(0).getTextContent());
                book.setPrice(Float.parseFloat(element.getElementsByTagName("price").item(0).getTextContent()));
                book.setPublish(DateUtil.parseDate(element.getElementsByTagName("publish_date").item(0).getTextContent()));
                books.add(book);
            }
        }
        return books;
    }


    public Collection<Book> getBookWithNameOBrien() {
        Collection<Book> books = null;
        Document document = getXmlDocument();
        XPath xPath = XPathFactory.newInstance().newXPath();
        NodeList nodeList;
        try {
            nodeList = (NodeList) xPath.compile(xPathExpression).evaluate(document, XPathConstants.NODESET);
            books = parseBook(nodeList);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        return books;
    }



    public Catalog unmarshalXml() {
        Catalog catalog = null;
        try (InputStream inputStream = BookDOMParser.class.getClassLoader().getResourceAsStream(filePath)) {
            JAXBContext jaxbContext = JAXBContext.newInstance(Catalog.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            catalog = (Catalog) jaxbUnmarshaller.unmarshal(inputStream);
        } catch (JAXBException | IOException e) {
            e.printStackTrace();
        }
        return catalog;
    }

    private Document getXmlDocument() {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        Document doc = null;
        try (InputStream inputStream = BookDOMParser.class.getClassLoader().getResourceAsStream(filePath)) {
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(inputStream);
        } catch (IOException | SAXException | ParserConfigurationException e) {
            e.printStackTrace();
        }
        doc.getDocumentElement().normalize();
        return doc;
    }


}
