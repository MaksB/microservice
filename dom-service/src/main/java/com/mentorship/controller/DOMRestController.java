package com.mentorship.controller;

import com.mentorship.model.Book;
import com.mentorship.model.Catalog;
import com.mentorship.parser.BookDOMParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

/**
 * Created by Maks on 12.11.2016.
 */
@RestController
@RequestMapping(value = "/dom")
public class DOMRestController {

    @Autowired
    private BookDOMParser domParser;

    @GetMapping("/catalog")
    public Catalog getCatalog() {
        return domParser.parseFile();
    }

    @GetMapping("/catalog/unmarshal")
    public Catalog getCatalogUnmarshal() {
        return domParser.unmarshalXml();
    }

    @GetMapping("/books")
    public Collection<Book> getBooks() {
        return domParser.getBookWithNameOBrien();
    }


}
